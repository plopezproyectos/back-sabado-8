//creamos el módulo a exportar
//Al ser llamado en index.js recibe las capacidades de express, para ser utilizado
module.exports = function (app) {
    app.get("/ensalada/:cantidad/:componenteUno/:componenteDos", async function(req, res){
    
        let cantidad = req.params.cantidad
        let componenteUno = req.params.componenteUno
        let componenteDos = req.params.componenteDos
    
        res.send("Tengo, aca sus " + cantidad + " ensaladas " + " con " + componenteUno + " y " + componenteDos)
    })
    
    app.get("/ensalada", async function(req, res){
    
        res.send("una sola ensalada grande")
    })
    
    app.get("/asado", async function(req, res){

        res.send("Tengo y muy bueno")
    })

    app.get("/productos", async function(req, res){

        const productos = require("./../services/productosServices")

        const response = await productos.getProductos()
        
        res.send(response.result)
    })
}
