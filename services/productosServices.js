//requerimos el módulo para conectarse a la base de datos
const mysql = require('mysql')
//requerimos el archivo donde tenemos configurada la conexion
const conn = require('../config/conn')

//creamos la constante a ser exportada
const productos = {

    //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
    async getProductos () {

        //Guardamos en una variable la consulta que queremos generar
        let sql = 'SELECT * FROM productos'
        //Con el archivo de conexion a la base, enviamos la consulta a la misma
        //Ponemos un await porque desconocemos la demora de la misma
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },

    async getProductosById (id) {

        let sql = 'SELECT * FROM productos WHERE id = ' + id
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },

    async potProductos(nuevo) {

        let sql = "INSERT INTO `test`.`productos` ( `id` ,`name` ,`direccion` ,`telefono` ,`foto` ,`celular` ,`correo`)VALUES (NULL , '"+ nuevo.name + "', '"+ nuevo.direccion + "', '"+ nuevo.telefono + "', '"+ nuevo.foto + "', '"+ nuevo.celular + "', '"+ nuevo.correo + "');"
           
        let resultado = await conn.query(sql)
        let response = {error: "usuario creado"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },
}
//Exportamos el módulo
module.exports = productos
